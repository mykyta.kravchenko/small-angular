function ngBind() {
  return {
    link: (scope, el) => {
      const { value } = el.attributes['ng-bind'];

      function updateTextContent() {
        el.textContent = scope.eval(value);
      }

      updateTextContent();
      scope.$watch(() => value, el, updateTextContent);
    }
  };
}

export default ngBind;
