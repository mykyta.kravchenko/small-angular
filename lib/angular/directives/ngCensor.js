const wordsToCensor = ['fuck', 'shit', 'cunt', 'retard', 'bastard', 'asshole', 'nigger', 'dick', 'pussy'];

function ngCensor() {
  return {
    link(scope, el) {
      wordsToCensor.forEach(word => {
        const regexp = new RegExp(word, 'gi');
        el.innerHTML = el.innerHTML.replace(regexp, match => '*'.repeat(match.length));
      });
    }
  };
}

export default ngCensor;
