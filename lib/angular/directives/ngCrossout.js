function ngCrossout() {
  return {
    link(scope, el) {
      const { textDecoration } = getComputedStyle(el);

      el.addEventListener('mouseover', () => {
        el.style.textDecoration = 'line-through';
      });

      el.addEventListener('mouseout', () => {
        el.style.textDecoration = textDecoration;
      });
    }
  };
}

export default ngCrossout;
