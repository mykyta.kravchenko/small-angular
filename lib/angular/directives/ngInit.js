function ngInit() {
  return {
    link(scope, el) {
      const { value } = el.attributes['ng-init'];
      scope.eval(value);
    }
  };
}

export default ngInit;
