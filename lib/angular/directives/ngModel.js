function ngModel() {
  return {
    link: (scope, el) => {
      const { value } = el.attributes['ng-model'];
      const attr = el.type === 'checkbox' ? 'checked' : 'value';

      if (!(value in scope)) {
        scope.eval(`${value} = '${el[attr]}'`);
      }

      const setInputProp = () => {
        el[attr] = scope.eval(value);
      };

      setInputProp();

      scope.$watch(value, el, setInputProp);

      el.addEventListener('input', ({ target }) => {
        scope.eval(`${value} = '${target[attr]}'`);
        scope.$applyAsync();
      });
    }
  };
}

export default ngModel;
