function ngRepeat() {
  return {
    link: (scope, el) => {
      const { value: expression } = el.attributes['ng-repeat'];
      const [item, iterable] = expression.split(/\s*in\s*/);

      const { parentNode, style } = el;
      const nodeCopy = el.cloneNode(true);
      style.display = 'none';

      function repeatElementAndCompile() {
        document.querySelectorAll('[ng-repeat-for-delete]').forEach(el => el.remove());

        for (const value of scope.eval(iterable)) {
          let nodeClone = nodeCopy.cloneNode(true);
          nodeClone.setAttribute('ng-repeat-for-delete', '');
          nodeClone.removeAttribute('ng-repeat');
          scope[item] = value;

          nodeClone = nodeClone.outerHTML.replace(/{{(.*?)}}/g, (match, p1) =>
            scope.$$parse(scope, p1));

          parentNode.insertAdjacentHTML('beforeend', nodeClone);

          parentNode.lastChild.querySelectorAll('*').forEach(scope.$$compile);
          scope.$$compile(parentNode.lastChild);
        }
      }

      scope.$watch(expression, el, repeatElementAndCompile);
    }
  };
}

export default ngRepeat;
