function toLowerCase(string) {
  return string.toLowerCase();
}

export default toLowerCase;
