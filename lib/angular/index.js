import * as baseDirectives from './directives';
import * as baseServices from './services';
import * as baseFilters from './filters';
import { toCamelCase, getDepNames } from './utils';
import pubSub from './pubSub';

function SmallAngular() {
  const $rootScope = window;
  const directives = { ...baseDirectives };
  const filters = { ...baseFilters };
  const services = {};
  const components = {};
  const controllers = {};
  const configs = [];
  let watchers = [];

  Object.assign($rootScope.__proto__, pubSub); // eslint-disable-line no-proto

  const getDependencies = fn => getDepNames(fn).map(name => {
    if (name === '$rootScope') {
      return $rootScope;
    }

    if (!services[name]) {
      throw new Error(`No such dependency: ${name}`);
    }

    return services[name];
  });

  const splitAttributes = attributes => {
    const attrs = {};
    const ngAttrs = {};

    for (const attr of attributes) {
      const { value, name } = attr;
      const nameInCamelCase = toCamelCase(name);

      if (directives[nameInCamelCase]) {
        ngAttrs[nameInCamelCase] = value;
      } else {
        attrs[name] = value;
      }
    }

    return [ngAttrs, attrs];
  };


  function service(name, fn) {
    const dependencies = getDependencies(fn);
    services[name] = fn(...dependencies);

    return this;
  }

  function constant(name, value) {
    services[name] = value;

    return this;
  }

  function filter(name, fn) {
    filters[name] = fn;

    return this;
  }

  function config(fn) {
    configs.push(fn);

    return this;
  }

  function directive(name, fn) {
    directives[name] = fn;

    return this;
  }

  function controller(name, fn) {
    controllers[name] = fn;

    return this;
  }

  function component(name, fn) {
    components[name.toLowerCase()] = fn;

    return this;
  }

  this.createApp = appName => {
    this.appName = appName;

    return this;
  };

  const compileComponent = (component, node, attrs) => {
    const dependencies = getDependencies(component);
    const {
      controller: controllerName,
      controllerAs,
      template,
      link
    } = component(...dependencies);

    if (template) {
      node.innerHTML = template;
    }

    if (!controllerName) {
      node.querySelectorAll('*').forEach(compile);
      link($rootScope, node, attrs);

      return;
    }

    const Controller = controllers[controllerName];

    if (!Controller) {
      throw new Error(`No such controller: ${controllerName}`);
    }

    const ctrlDependecies = getDependencies(Controller);
    const ctrlInstance = new Controller(...ctrlDependecies);

    if (controllerAs) {
      $rootScope[controllerAs] = ctrlInstance;
      node.querySelectorAll('*').forEach(compile);
    }

    if (!link) {
      return;
    }

    link($rootScope, node, attrs, ctrlInstance);
  };

  const compile = node => {
    const { attributes, tagName } = node;
    const [ngAttrs, attrs] = splitAttributes(attributes);
    const componentName = tagName.toLowerCase();
    const component = components[componentName];

    if (component) {
      compileComponent(component, node, attrs);
    }

    for (const name in ngAttrs) {
      const dependencies = getDependencies(directives[name]);

      directives[name](...dependencies).link($rootScope, node, attrs);
    }
  };

  const parse = function(scope, string) {
    const [expression, ...filtersArray] = string.split('|').map(str => str.trim());
    const expressionResult = scope.eval(expression);

    return filtersArray.reduce((acc, filterName) => filters[filterName](acc), expressionResult);
  };

  $rootScope.$$compile = compile;
  $rootScope.$$parse = parse;

  $rootScope.$watch = (_, node, fn) => {
    watchers.push({ node, fn });
  };

  $rootScope.$apply = () => {
    watchers = watchers.filter(({ node: { isConnected }, fn }) => {
      if (isConnected) {
        fn();
      }

      return isConnected;
    });
  };

  $rootScope.$applyAsync = () => setTimeout($rootScope.$apply);

  const initBaseServices = () => {
    for (const name in baseServices) {
      service(name, baseServices[name]);
    }
  };

  const initConfigs = () => configs.forEach(fn => {
    const dependencies = getDependencies(fn);
    fn(...dependencies);
  });

  this.bootstrap = (node = document.querySelector('[ng-app]')) => {
    initConfigs();
    node.querySelectorAll('*').forEach(compile);
  };

  this.directive = directive;
  this.component = component;
  this.service = service;
  this.controller = controller;
  this.constant = constant;
  this.filter = filter;
  this.config = config;

  initBaseServices();
  setTimeout(this.bootstrap);
}

window.angular = new SmallAngular();
