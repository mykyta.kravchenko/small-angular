const eventsSymbol = Symbol('events');

class PubSub {
  constructor() {
    this[eventsSymbol] = {};
    this.publish = function(eventName, ...args) {
      if (!this[eventsSymbol][eventName]) {
        return;
      }

      this[eventsSymbol][eventName].forEach(cb => cb(...args));
    };
    this.subscribe = function(eventName, callback) {
      if (!this[eventsSymbol][eventName]) {
        this[eventsSymbol][eventName] = [];
      }

      this[eventsSymbol][eventName].push(callback);
    };
  }
}

export default new PubSub();
