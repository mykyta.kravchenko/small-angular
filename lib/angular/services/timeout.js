function $timeout($rootScope) {
  'inject';

  return (fn, timeToDelay, ...args) => {
    setTimeout(() => {
      fn(...args);
      $rootScope.$applyAsync();
    }, timeToDelay);
  };
}

export default $timeout;
