function getDependenciesNames(fn) {
  const fnString = fn.toString();
  const ifInject = (/\)[\s\n]*{[\n\s]*('inject')/).test(fnString);

  if (!ifInject || !fn.length) {
    return [];
  }

  const [, args] = fnString.match(/\(([^)]*)\)/s);

  return args.split(',').map(element => element.trim());
}

export default getDependenciesNames;
