function toCamelCase(string) {
  return string.replace(/-([a-z])/g, (match, p1) => p1.toUpperCase());
}

export default toCamelCase;
